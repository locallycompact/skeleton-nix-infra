{
  network.description = "cache-server";

  webserver =
    { config, pkgs, ... }:
    { services.httpd.enable = true;
      services.httpd.adminAddr = "alice@example.org";
      services.httpd.documentRoot = "${pkgs.valgrind.doc}/share/doc/valgrind/html";
      networking.firewall.allowedTCPPorts = [ 80 5000 ];
      environment.systemPackages = [ pkgs.nix-serve ];
      users.extraUsers.root.openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKticimjjptZtTvRycr6hwhKsYDl4MQKhIE76YxuK1DmZcJDu692tMEBtlx2PW1M6hp2t73nxcLIJO5oolVQxbR2Cfhz1VqVDh12F/xM0gjhWHy1J1wHorptTZQNqtN+lkjde7/7JFMEjidw46bOoCGeabQRsE595cig1mFengOIao6io/F3TCVV72noEdhPeuPAgD49hZkHQ1wczsjI0w+5dpknJ/WoNZQsNISHmBbtUe0uPpHU9dHZHm6G+DWyDU3L6RrQbaj/7G1UhFvEEL3eeBDWmydJYZg2lEfL4LGKOOgUtgLue33wiCmtsp+dqLhPyN+f0Wo+wFLYw5Vda1 cache-service@webserver"
      ];
      services.nix-serve.enable = true;
   };
}
