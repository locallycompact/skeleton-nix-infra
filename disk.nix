let 
  pkgs = import <nixpkgs> { };
  lib = pkgs.lib;
  x86_config = { pkgs, ... }:
  {
    fileSystems = {
      "/" = { device = "LABEL=nixos"; fsType = "ext4"; };
    };
    boot = {
      loader.grub = {
        enable = true;
        device = "/dev/vda";
      };
    };
    environment.systemPackages = [ pkgs.bustle pkgs.qt5.full ];
    services.mingetty.autologinUser = "root";
  };
  x86_build = import <nixpkgs/nixos> { configuration = x86_config; };
  rootDisk = import <nixpkgs/nixos/lib/make-disk-image.nix> {
    name = "nix_x86_rootfs";
    inherit pkgs lib;
    config = x86_build.config;
    diskSize = 4096;
  };
in rootDisk
