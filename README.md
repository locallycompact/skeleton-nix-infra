# Skeleton Nix Infrastructure

This is a basic build-y cache server-y nix infrastructure. It's production capable
but a bit minimal atm.

# Building

Make sure you can build the rootfs locally:

    nix-build disk.nix

# Deploying Cache Server

    cd cache-server
    nixops create -d cache cache-server.nix cache-server-vbox.nix
    nixops deploy -d cache

Wait for this to completely finish.

# Copying local files to cache server

Get the ip address of the cache server with

    nixops info -d cache

Use the ssh key in the cache-server directory to push the closure across, substituting the ip
for your virtualbox ip.

    NIX_SSHOPTS='-i id_rsa' nix-copy-closure --to root@192.168.56.101 $(type -p nix_x86_rootfs)
